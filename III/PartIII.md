
# III. Manipulations d'autres outils/protocoles côté client
1. DHCP 
* ``$ ip r``
```bash
$ ip r     
default via 10.33.3.253 dev wlan0 proto dhcp metric 600 
10.33.0.0/22 dev wlan0 proto kernel scope link src 10.33.3.243 metric 600 
```
* l'addresse etant ici `10.33.3.253`
* DHCP lease:  ```ip a | 'sec'```
```bash
parrot% ip a | grep 'sec'
        valid_lft 1859sec preferred_lft 1859sec
```
* il me reste 1859 secondes
---
1. DNS
* ``` $ dig google.com```
  ```bash
  ;; ANSWER SECTION:
    google.com.		236	IN	A	172.217.18.206
    ```
* ``$ nslookup google.com``
  ```bash 
    Server:		89.2.0.1
    Address:	89.2.0.1#53

    Non-authoritative answer:
    Name:	google.com
    Address: 172.217.18.206
    Name:	google.com
    Address: 2a00:1450:4007:805::200e
    ```

* ``$ nslookup ynov.com``
  ```bash
    Server:		89.2.0.1
    Address:	89.2.0.1#53

    Non-authoritative answer:
    Name:	ynov.com
    Address: 217.70.184.38
    ```