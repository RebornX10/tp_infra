# Affichage d'informations sur la pile TCP/IP locale

## 1. Affichage d'informations sur la pile TCP/IP locale
 * ```$ ifconfig wlan0``` 
  ```bash
  wlan0 || b4:6b:fc:9f:07:5e || 10.33.3.243 
  ```
 * ```$ ifconfig eth0```


```bash
    eth0: error fetching interface information: Device not found
```
    sorryj'aipasdecarteethernet

 * ```$ netstat -nr```
```bash
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         10.33.3.253     0.0.0.0         UG        0 0          0 wlan0
10.33.0.0       0.0.0.0         255.255.252.0   U         0 0          0 wlan0
```
* La gateway sur le reseau d'Ingesup sert a envoyer des paquets hors du reseau ``¯\_(ツ)_/¯``

* ![](GUI.png?raw=true "GUI")

---
---
## 2. Modifications des informations
1. 
 * ![](ipchange.png?raw=true "GUI")
 * j'ai perdu l'acces reseau parceque je n'ai pas scanner les ips avant.

2. 
```bash
parrot% nmap -sP 10.33.0.0/22    
Starting Nmap 7.80 ( https://nmap.org ) at 2020-01-16 17:02 UTC
RTTVAR has grown to over 2.3 seconds, decreasing to 2.0
Nmap scan report for 10.33.0.11
Host is up (0.0083s latency).
Nmap scan report for 10.33.0.15
Host is up (0.11s latency).
Nmap scan report for 10.33.0.16
Host is up (0.42s latency).
Nmap scan report for 10.33.0.19
Host is up (0.037s latency).
Nmap scan report for 10.33.0.29
Host is up (0.097s latency).
Nmap scan report for 10.33.0.35
...
...
...
Host is up (0.00019s latency).
Nmap scan report for 10.33.3.253
Host is up (0.0086s latency).
Nmap done: 1024 IP addresses (166 hosts up) scanned in 77.39 seconds
```