# II. Exploration locale en duo

* ![](1.png)
  
* 
```bash
$  ping 192.168.137.1

    PING 192.168.137.1 (192.168.137.1) 56(84) bytes of data.
    64 bytes from 192.168.137.1: icmp_seq=1 ttl=128 time=1.60 ms
    64 bytes from 192.168.137.1: icmp_seq=2 ttl=128 time=1.10 ms
    64 bytes from 192.168.137.1: icmp_seq=3 ttl=128 time=1.65 ms
    ^C
    --- 192.168.137.1 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2003ms
    rtt min/avg/max/mdev = 1.097/1.445/1.645/0.247 ms
```
* ``$ ip a``
```bash 
    8: enx00e04c360f27: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:e0:4c:36:0f:27 brd ff:ff:ff:ff:ff:ff
    inet 192.168.137.2/24 brd 192.168.137.255 scope global noprefixroute enx00e04c360f27
```

* ``# dig https://www.reddit.com/``
```bash

    ; <<>> DiG 9.11.5-P4-5.1+b1-Debian <<>> https://www.reddit.com/
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 20132
    ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 512
    ;; QUESTION SECTION:
    ;https://www.reddit.com/.	IN	A

    ;; AUTHORITY SECTION:
    .			86395	IN	SOA	a.root-servers.net. nstld.verisign-grs.com.     2020012300 1800 900 604800 86400

    ;; Query time: 28 msec
    ;; SERVER: 8.8.8.8#53(8.8.8.8)
    ;; WHEN: Thu Jan 23 17:24:11 UTC 2020
    ;; MSG SIZE  rcvd: 127
```

* ``$ traceroute google.com``
```bash
    traceroute to google.com (172.217.19.238), 30 hops max, 60 byte packets
     1  _gateway (192.168.137.1)  1.313 ms  1.248 ms  1.195 ms
     2  * * *
     3  10.33.3.253 (10.33.3.253)  7.098 ms  7.046 ms  6.995 ms
     4  reverse.completel.net (92.103.174.137)  6.902 ms  8.053 ms  9.163 ms
     5  92.103.120.182 (92.103.120.182)  10.992 ms  11.988 ms  11.955 ms
     6  172.19.130.117 (172.19.130.117)  22.317 ms  25.249 ms  24.719 ms
     7  46.218.96.63 (46.218.96.63)  18.211 ms  18.070 ms  18.990 ms
     8  108.170.245.1 (108.170.245.1)  24.347 ms 108.170.244.193    (108.170.244.193)  24.203 ms  24.153 ms
     9  216.239.59.209 (216.239.59.209)  20.285 ms 216.239.59.211   (216.239.59.211)  20.868 ms  23.945 ms
    10  par21s11-in-f14.1e100.net (172.217.19.238)  23.925 ms  23.833 ms    20.405 ms
```

* ``  └──╼ #nc 192.168.137.1 8888``
```bash 
as
on tal
onTest
 parrle wowowlo
C'est mieux que PictoChat
on parrle wowowlo
```

![](https://i.imgur.com/F8SWw8E.png)

* ![](5.png)
l'image ci-dessus est un netcat  
* ![](6.png)
  l'image ci-dessus est un ping(pleiiiin de ping)
